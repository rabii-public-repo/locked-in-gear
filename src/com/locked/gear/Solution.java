package com.locked.gear;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	private static final String NOT_MOVING = "NOT MOVING";
	private static final String COUNTER_CLOCK_WISE = "CCW";
	private static final String CLOCK_WISE = "CW";

	/* === get only attached gears to the first gear from list === */
	static List<Gear> getAttachedGearToFirstGear(Gear firstGear, List<Gear> gears) {
		List<Gear> attachedGears = new ArrayList<Gear>();
		attachedGears.add(firstGear);
		gears.remove(firstGear);
		int distanceBetweenTwoGears = 0;
		int sumRadius = 0;
		boolean attached = false;
		while (!attached) {
			attached = true;
			Gear gear = null;
			for (int j = 0; j < gears.size(); j++) {
				gear = gears.get(j);

				distanceBetweenTwoGears = (int) (Math.pow(firstGear.getX() - gear.getX(), 2)
						+ Math.pow(firstGear.getY() - gear.getY(), 2));
				sumRadius = (int) (Math.pow(firstGear.getRadius() + gear.getRadius(), 2));
				if (sumRadius == distanceBetweenTwoGears) {
					attachedGears.add(gear);
					gears.remove(gear);
					attached = false;
					break;
				}
			}
			if (sumRadius == distanceBetweenTwoGears) {
				firstGear = gear;
			} else {
				return attachedGears;
			}
		}
		return attachedGears;
	}

	/* === set directions of list of gear === */
	static void getDirectionOfLastGear(Gear lastGear, List<Gear> attachedGears) {
		if (attachedGears.size() == 1 || !attachedGears.contains(lastGear) || checkMultipleTouchs(attachedGears)) {
			lastGear.setDirection(NOT_MOVING);
		} else {
			for (int i = 1; i < attachedGears.size(); i++) {
				Gear gear = attachedGears.get(i);
				if (gear.getDirection() == "") {
					if (i % 2 == 0) {
						gear.setDirection(CLOCK_WISE);
					} else {
						gear.setDirection(COUNTER_CLOCK_WISE);
					}
				} else {
					lastGear.setDirection(NOT_MOVING);
					break;
				}
			}
		}
		System.out.println(lastGear.getDirection());
	}

	/* === check if we have 3 gears touchs each other=== */
	static boolean checkMultipleTouchs(List<Gear> touchedGears) {
		Gear firstGear = touchedGears.get(0);
		if (touchedGears.size() > 2) {
			for (int i = 1; i < touchedGears.size(); i++) {
				Gear secondGear = touchedGears.get(i);
				Gear thirdGear = null;
				if (i + 1 < touchedGears.size()) {
					thirdGear = touchedGears.get(i + 1);
				} else {
					return false;
				}
				int AB = firstGear.getRadius() + secondGear.getRadius();
				int AC = secondGear.getRadius() + thirdGear.getRadius();
				int BC = firstGear.getRadius() + thirdGear.getRadius();
				if (Math.pow(AB, 2) + Math.pow(AC, 2) == Math.pow(BC, 2)) {
					return true;
				}
				firstGear = touchedGears.get(i);
			}
		}
		return false;
	}
}

class Gear {
	private Integer x;
	private Integer y;
	private Integer radius;
	private String direction;

	public Gear() {
	}

	public Gear(Integer x, Integer y, Integer radius, String direction) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Gear [x=" + x + ", y=" + y + ", radius=" + radius + ", direction=" + direction + "]";
	}

	Integer getX() {
		return x;
	}

	void setX(Integer x) {
		this.x = x;
	}

	Integer getY() {
		return y;
	}

	void setY(Integer y) {
		this.y = y;
	}

	Integer getRadius() {
		return radius;
	}

	void setRadius(Integer radius) {
		this.radius = radius;
	}

	String getDirection() {
		return direction;
	}

	void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + ((radius == null) ? 0 : radius.hashCode());
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gear other = (Gear) obj;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (radius == null) {
			if (other.radius != null)
				return false;
		} else if (!radius.equals(other.radius))
			return false;
		if (x == null) {
			if (other.x != null)
				return false;
		} else if (!x.equals(other.x))
			return false;
		if (y == null) {
			if (other.y != null)
				return false;
		} else if (!y.equals(other.y))
			return false;
		return true;
	}
}
